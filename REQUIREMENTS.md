# BACKEND INTERNSHIP PROGRAM

## 1. Yêu cầu
Bạn hãy thiết kế cơ sở dữ liệu, thiết kế API và viết code để xây dựng ứng dụng quản lý công việc với các tính năng dưới đây.

**Mô tả:**
- Một người dùng(User) có thể đăng ký tài khoản với các thông tin(tên, email, mật khẩu).
- Mật khẩu của người dùng cần được bảo vệ bằng cách mã hóa, để đảm bảo dù có bị lộ thì mật khẩu cũng không thể đoán được.
- Người dùng có thể đăng nhập vào hệ thống bằng email và mật khẩu đã đăng ký.
- Sau khi đăng nhập thành công, người dùng có thể tạo mới Dự án(Project).
- Một người dùng có nhiều Project. Mỗi Project có các thông tin sau:
  - Tên của project: *bắt buộc. là tên của dự án đang thực hiện
  - Thông tin chi tiết của project: Giới thiệu các thông tin bổ sung cho dự án.

- Trong mỗi Project, người dùng có thể thêm, sửa thông tin và xóa các Công việc(Task).
- Một Project có nhiều Tasks. Mỗi Task có các thông tin sau:
  - Tiêu đề của Task: Một Task phải có tiêu đề, tối đa 50 từ, mô tả gắn gọn việc sẽ làm
  - Thông tin chi tiết của Task: Chi tiết nội dung việc cần làm, tối đa chỉ được 1000 từ.
  - Ngày giờ đến hạn của Task: Khi tạo Task có thể chọn ngày hết hạn hoặc không. Nếu không chọn thì mặc định là cuối ngày hôm nay.
  - Trạng thái của Task: Một Task có 2 trạng thái là "Chưa hoàn thành" và "Hoàn thành". Mặc định khi tạo task là "Chưa hoàn thành"
  - Ngày hoàn thành của Task: Là thời điểm task được chuyển sang trạng thái "Hoàn thành". Mặc định là rỗng khi tạo task.

- Mỗi người dùng chỉ có thể xem được các Project và Task của mình.
- Người dùng có thể xóa các Project của mình. Nếu xóa Project, các Task trong Project đó cũng bị xóa theo.
- Người dùng có thể xem danh sách các Task trong một Project của mình theo trạng thái(toàn bộ, hoàn thành, chưa hoàn thành).
- Người dùng có thể đánh dấu Task đã hoàn thành.
- Khi người dùng đánh dấu Task là hoàn thành thì:
  - Ngày hoàn thành của Task sẽ được cập nhật thành thời điểm đánh dấu hoàn thành.
  - Gửi thông báo cho người dùng, thông báo có một Task vừa hoàn thành.
- Người dùng có thể đánh dấu Task chưa hoàn thành.
- Khi người dùng đánh dấu Task là chưa hoàn thành thì:
  - Ngày hoàn thành của Task sẽ được cập nhật thành rỗng.
  - Gửi thông báo cho người dùng là Task được cập nhật thành "Chưa hoàn thành"
- Nếu Task có ngày giờ đến hạn, 15 phút trước khi đến hạn cần thông báo cho người dùng biết là Task sắp hết hạn.
- Nếu thời điểm tạo Task tới thời điểm hết hạn nhỏ hơn 15 phút thì không cần thông báo.
- Định kỳ 8h sáng hàng ngày, thông báo cho người dùng:
  - Số lượng Task đã tới hạn mà chưa hoàn thành.
  - Số lượng Task cần hoàn thành của ngày hôm đó.
- Mọi thông báo được gửi qua Telegram

**Output cần báo cáo:**
- Source code của app
- File README.md với giới thiệu kiến trúc, Hướng dẫn cài đặt từ khi chưa có gì tới khi chạy được ứng dụng.
- (Không bắt buộc)API Document sử dụng OpenAPI, yêu cầu có file specs.json hoặc specs.yaml
- (Không bắt buộc)Sơ đồ ERD(Entity Relationship Diagram) của cơ sở dữ liệu bạn đã thiết kế, và giải thích lý do tại sao lại thiết kế như vậy

**Tham khảo:**
- Ngôn ngữ: Python3.10
- API Framework: Flask/FastAPI
  - Flask(Ưu tiên sử dụng): https://flask.palletsprojects.com/en/3.0.x/
  - FastAPI: https://fastapi.tiangolo.com/
- Database: PostgreSQL
  - Psycopg2: https://wiki.postgresql.org/wiki/Psycopg2_Tutorial
  - SQLAlchemy: https://www.sqlalchemy.org/
- Xử lý thông báo chạy ngầm:
  - Celery: https://docs.celeryq.dev/en/stable/getting-started/introduction.html
  - Python RQ: https://python-rq.org/
- Thông báo qua telegram:
  - API(Ưu tiên sử dụng): https://core.telegram.org/bots/api#sendmessage
  - Python SDK: https://github.com/python-telegram-bot/python-telegram-bot
- OpenAPI(Không bắt buộc): 
  - Cú pháp: https://swagger.io/specification/
  - Giao diện: https://swagger.io/tools/swagger-ui/
- Cấu trúc code tham khảo: https://github.com/teamhide/fastapi-boilerplate
- Sách: Python 3 Object-oriented Programming
- Các thư viện khác: Bạn có thể sử dụng các thư viện khác nếu cần thiết
Ngoài các nguồn tham khảo trên, bạn có thể tự tìm hiểu các nguồn khác để đáp ứng yêu cầu.  


## 2. Hướng dẫn
1. Tạo branch mới từ branch `master` và đặt tên theo tên của mình. Ví dụ bạn tên là "Nguyễn Văn A" thì tạo branch mới với tên là ` nguyen_van_a`.  
2. Sau khi hoàn thành các yêu cầu(ở phần output cần báo cáo), bạn push các file này lên branch `nguyen_van_a`.
3. Sau đó, bạn cần tạo Merge request từ branch `nguyen_van_a` đến branch `master`. Bạn gửi Merge Request này cho mentor.  
4. Mentor sẽ review, yêu cầu chỉnh sửa nếu có.  
5. Nếu cần chỉnh sửa, bạn chỉnh sửa trên branch đang làm việc, không cần tạo branch mới. Khi chỉnh sửa xong nhấn nút yêu cầu review. Bước 4, 5 sẽ liên tục lặp lại cho tới khi không cần phải sửa.
6. Hoàn thành
